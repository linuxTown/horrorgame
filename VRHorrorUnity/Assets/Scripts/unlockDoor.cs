﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class unlockDoor : OVRGrabbable
{
    public GameObject head;
    public OnDoorHandleTrigger doorHandle;

    public override void GrabEnd(Vector3 linearVelocity, Vector3 angularVelocity)
    {
        base.GrabEnd(linearVelocity, angularVelocity);

        transform.position = head.transform.position;
        transform.rotation = head.transform.rotation;
    }

    // Update is called once per frame
    private void Update()
    {
        if (Vector3.Distance(head.transform.position, transform.position) > 0.5f)
        {
            grabbedBy.ForceRelease(this);
            doorHandle.lockDoor(false);
            //play unlock sound
        }
    }
}
