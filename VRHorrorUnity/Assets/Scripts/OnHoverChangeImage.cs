﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class OnHoverChangeImage : MonoBehaviour
{
    [SerializeField]
    Sprite normalImg;

    [SerializeField]
    Sprite hoverImg;

    Image currentComponent;

    // Start is called before the first frame update
    void Start()
    {
        currentComponent = GetComponent<Image>();
    }

    public void OnHover()
    {
        currentComponent.sprite = hoverImg;
    }

    public void OnHoverExit()
    {
        currentComponent.sprite = normalImg;
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
