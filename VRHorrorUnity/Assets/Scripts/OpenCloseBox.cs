﻿using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

public class OpenCloseBox : MonoBehaviour
{
    bool rotating = false;
    public GameObject objectToRotate;
    public bool isOpen;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.O))
        {
            OpenCloseLid();
        }
    }

    public void OpenCloseLid()
    {
        if (isOpen)
        {
            Quaternion rotation = Quaternion.Euler(new Vector3(0, -180, 0));
            StartCoroutine(rotateObject(objectToRotate, rotation, 2f, false));
        }
        else
        {
            Quaternion rotation = Quaternion.Euler(new Vector3(0, -180, -80));
            StartCoroutine(rotateObject(objectToRotate, rotation, 2f, true));
        }
    }

    IEnumerator rotateObject(GameObject gameObjectToMove, Quaternion newRot, float duration, bool open)
    {
        if (rotating)
        {
            yield break;
        }
        rotating = true;

        Quaternion currentRot = gameObjectToMove.transform.rotation;

        float counter = 0;
        while (counter < duration)
        {
            counter += Time.deltaTime;
            gameObjectToMove.transform.rotation = Quaternion.Lerp(currentRot, newRot, counter / duration);
            yield return null;
        }
        rotating = false;
        isOpen = open;
    }
}

//[CustomEditor(typeof(OpenCloseBox))]
//public class customButton : Editor
//{
//    public override void OnInspectorGUI()
//    {
//        DrawDefaultInspector();

//        OpenCloseBox myScript = (OpenCloseBox)target;
//        if (GUILayout.Button("Open/close lid"))
//        {
//            myScript.OpenCloseLid();
//        }
//    }

//}
