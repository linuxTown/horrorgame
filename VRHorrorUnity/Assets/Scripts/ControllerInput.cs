﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class ControllerInput : MonoBehaviour
{
    //[System.Serializable]
    //public class Callback : UnityEvent<Ray, RaycastHit> { }

    public Transform leftHandAnchor = null;
    public Transform rightHandAnchor = null;
    public LineRenderer lineRenderer = null;
    public float maxRayDistance = 30.0f;
    //public VRRaycaster.Callback raycastHitCallback;
    OnHoverChangeImage lastHitButton = null;

    // Start is called before the first frame update
    void Start()
    {
        if (leftHandAnchor == null)
        {
            Debug.LogWarning("Assign LeftHandAnchor in the inspector!");
            GameObject left = GameObject.Find("LeftHandAnchor");
            if (left != null)
            {
                leftHandAnchor = left.transform;
            }
        }
        if (rightHandAnchor == null)
        {
            Debug.LogWarning("Assign RightHandAnchor in the inspector!");
            GameObject right = GameObject.Find("RightHandAnchor");
            if (right != null)
            {
                rightHandAnchor = right.transform;
            }
        }
        if (lineRenderer == null)
        {
            Debug.LogWarning("Assign a line renderer in the inspector!");
            // just create one then if it's not provided
            lineRenderer = rightHandAnchor.gameObject.AddComponent<LineRenderer>();
            lineRenderer.shadowCastingMode = UnityEngine.Rendering.ShadowCastingMode.Off;
            lineRenderer.receiveShadows = false;
            lineRenderer.widthMultiplier = 0.02f;
            lineRenderer.startColor = Color.white;
            lineRenderer.endColor = Color.white;
            Material whiteDiffuseMat = new Material(Shader.Find("Unlit/Texture"));
            lineRenderer.material = whiteDiffuseMat;
        }
    }

    Transform Pointer
    {
        get
        {
            OVRInput.Controller controller = OVRInput.GetConnectedControllers();
            if ((controller & OVRInput.Controller.LTrackedRemote) != OVRInput.Controller.None)
            {
                return leftHandAnchor;
            }
            else if ((controller & OVRInput.Controller.RTrackedRemote) != OVRInput.Controller.None)
            {
                return rightHandAnchor;
            }
            else // default
            {
                return rightHandAnchor;
            }
        }
    }

    // Update is called once per frame
    void Update()
    {
        OVRInput.Update();

        // returns true if the left index finger trigger has been pressed more than halfway.
        // (Interpret the trigger as a button).
        bool triggerPressed = OVRInput.GetDown(OVRInput.RawButton.RIndexTrigger);
        bool AButtonPressed = OVRInput.GetDown(OVRInput.RawButton.A);
        if (triggerPressed || AButtonPressed) {
            if(triggerPressed)
                Debug.Log("Trigger click event");
            else if(AButtonPressed)
                Debug.Log("A button was pressed"); // behave same as a trigger
            onTriggerPressed();
        }

        // line renderer:
        Transform pointer = Pointer;
        if (pointer == null)
        {
            return;
        }

        Ray laserPointer = new Ray(pointer.position, pointer.forward);

        if (lineRenderer != null)
        {
            lineRenderer.SetPosition(0, laserPointer.origin);
            lineRenderer.SetPosition(1, laserPointer.origin + laserPointer.direction * maxRayDistance);
        }


        RaycastHit hit;
        if (Physics.Raycast(laserPointer, out hit, maxRayDistance))
        {
            if (lineRenderer != null)
            {
                lineRenderer.SetPosition(1, hit.point); // end position same as collision point
            }

            OnHoverChangeImage newHitButton = hit.transform.gameObject.GetComponent<OnHoverChangeImage>();
            //string prevSelectedObjName = EventSystem.current.currentSelectedGameObject.name;
            if (newHitButton != null && (lastHitButton == null || newHitButton.name != lastHitButton.name))
            {
                deselectButtons();
                //newHitButton.Select();
                newHitButton.OnHover();
                lastHitButton = newHitButton;
            }
        }
        else if(lastHitButton != null) // no collider detected and old button is selected
        {
            deselectButtons();
            lastHitButton = null; // reset last hit button
        }
    }

    void deselectButtons()
    {
        OnHoverChangeImage[] btns = FindObjectsOfType<OnHoverChangeImage>();
        Debug.Log(btns + " : " + btns.Length);
        foreach (var btn in btns)
        {
            btn.OnHoverExit();
        }
    }

    void onTriggerPressed()
    {
        Debug.Log("Trigger pressed()");
        if (lastHitButton != null)
        {
            //lastHitButton.onClick.Invoke(); // execute click on this button directly or use execute events:
            //var pointer = new PointerEventData(EventSystem.current);
            //ExecuteEvents.Execute(lastHitButton.gameObject, pointer, ExecuteEvents.pointerDownHandler);
            //ExecuteEvents.Execute(lastHitButton.gameObject, new BaseEventData(EventSystem.current), ExecuteEvents.submitHandler);
            //Debug.Log("Event executed()");
        }
    }
}
