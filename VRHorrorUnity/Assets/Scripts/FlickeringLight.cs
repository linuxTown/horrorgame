using UnityEngine;
using System.Collections;

[RequireComponent(typeof(Light))]
public class FlickeringLight : MonoBehaviour {
	Light lt;
	float originalRange;

	[Space(10)]

	[Header("Light customization")]
	[Space(10)]
	[SerializeField]
	[Tooltip("The color of your light.")]
	Color lightColor = Color.yellow;

	[SerializeField]
	[Tooltip("The min intensity of your light.")]
	float minIntensity = 8f;
	[SerializeField]
	[Tooltip("The max intensity of your light.")]
	float maxIntensity = 3f;
	[SerializeField]
	[Tooltip("The minimal range of your light (radius).")]
	float minRange = 79f;
	[SerializeField]
	[Tooltip("The maximum range of your light (radius).")]
	float maxRange = 100f;

	[SerializeField]
	float minFlickerSpeed;

	[SerializeField]
	float maxFlickerSpeed;

	[SerializeField]
	float minTimeFullIntensity;

	[SerializeField]
	float maxTimeFullIntensity;

	void Start() {
		lt = GetComponent<Light>();
		lt.color = lightColor;
		StartCoroutine("flicker");

       
	}
	void FixedUpdate() {
		
	}

	IEnumerator flicker()
	{
		while (true)
		{
			lt.range = Random.Range(minRange, maxRange);
			lt.intensity = Random.Range(minIntensity, maxIntensity);
			yield return new WaitForSeconds(Random.Range(minFlickerSpeed, maxFlickerSpeed));
			lt.intensity = maxIntensity;
			yield return new WaitForSeconds(Random.Range(minTimeFullIntensity, maxTimeFullIntensity));
		}
	}
}