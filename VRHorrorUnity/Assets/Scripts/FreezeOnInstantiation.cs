﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FreezeOnInstantiation : MonoBehaviour
{
    // Start is called before the first frame update
    IEnumerator Start()
    {
        yield return new WaitForSeconds(1F);
        gameObject.GetComponent<Rigidbody>().isKinematic = true;
    }
}
