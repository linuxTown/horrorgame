﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GlassesClinking : MonoBehaviour
{
    private void OnCollisionEnter(Collision collision)
    {
        if (collision.relativeVelocity.magnitude < 2 && collision.relativeVelocity.magnitude > .15)
            gameObject.GetComponent<AudioSource>().Play();
    }
}
