﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GrabObject : MonoBehaviour
{
    private bool contact;
    List<GameObject> collisions;
    private bool grabbed = false;
    public GameObject grabbedObj;
    private bool release = false;
    private Rigidbody rb;

    // Start is called before the first frame update
    void Start()
    {
        contact = false;
        collisions = new List<GameObject>();
    }

    // Update is called once per frame
    void Update()
    {
        OVRInput.Update();
       
        if(grabbed)
        {
            if (!OVRInput.Get(OVRInput.RawButton.RHandTrigger) || release)
            {
                grabbed = false;
                release = false;
                if(grabbedObj.layer == 9)
                {
                    rb.velocity = Vector3.zero;
                    rb.angularVelocity = Vector3.zero;
                    GameObject[] grabbedObjParent = GameObject.FindGameObjectsWithTag("GrabbableParent");
                    Transform parentPosition = grabbedObjParent[0].transform; 
                    Rigidbody rbParent = grabbedObjParent[0].GetComponent<Rigidbody>();

                    grabbedObj.transform.position = parentPosition.position;
                    grabbedObj.transform.rotation = parentPosition.rotation;
                    rbParent.velocity = Vector3.zero;
                    rbParent.angularVelocity = Vector3.zero;
                }
                grabbedObj = null;
                rb = null;
            }
            else
            {
                grabbedObj.transform.position = transform.position;
                GameObject[] grabbedObjParent = GameObject.FindGameObjectsWithTag("GrabbableParent");
                if (Vector3.Distance(grabbedObjParent[0].transform.position,transform.position) > 0.2f)
                {
                    release = true;
                }
            }
        }
        else if (contact)
        {
            if (OVRInput.Get(OVRInput.RawButton.RHandTrigger))
            {
                foreach (GameObject obj in collisions)
                {

                    if (obj.tag == "Grabbable")
                    {
                        Debug.Log("object Grabbed");
                        grabbed = true;
                        grabbedObj = obj;
                        rb = grabbedObj.GetComponent<Rigidbody>();
          

                    }
                    
                }

            }
        }

    }


    void OnTriggerEnter(Collider other)
    {
        contact = true;
        collisions.Add(other.gameObject);
    }

    private void OnTriggerExit(Collider other)
    {
        contact = false;
        collisions.Remove(other.gameObject);
    }
}
