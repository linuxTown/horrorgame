﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ChangeSkybox : MonoBehaviour
{
    [SerializeField]
    private Material skyboxMaterial;
    // Start is called before the first frame update
    void Start()
    {
        if(skyboxMaterial != null)
        {
            RenderSettings.skybox = skyboxMaterial;
        }
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
