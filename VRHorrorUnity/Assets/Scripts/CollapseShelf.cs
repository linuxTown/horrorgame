﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CollapseShelf : MonoBehaviour
{
    public GameObject topShelf;
    public GameObject secondShelf;
    public GameObject thirdShelf;
    public GameObject fourthShelf;
    public GameObject rightPoles;
    private float time = .5F;
    public bool trigger = false;
    private int counter = 0;

    // Update is called once per frame
    void FixedUpdate()
    {
        StartCoroutine("doThing");
    }

    IEnumerator doThing()
    {
        if(trigger && counter >= 0)
        {
            counter++;
            topShelf.GetComponent<Rigidbody>().isKinematic = false;
            rightPoles.GetComponent<Rigidbody>().isKinematic = false;
            yield return new WaitForSeconds(.9F);
            secondShelf.GetComponent<Rigidbody>().isKinematic = false;
            yield return new WaitForSeconds(time);
            thirdShelf.GetComponent<Rigidbody>().isKinematic = false;
            yield return new WaitForSeconds(.03F);
            fourthShelf.GetComponent<Rigidbody>().isKinematic = false;
            yield return new WaitForSeconds(.02F);
            topShelf.GetComponent<Rigidbody>().isKinematic = true;
            rightPoles.GetComponent<Rigidbody>().isKinematic = true;
            secondShelf.GetComponent<Rigidbody>().isKinematic = true;
            thirdShelf.GetComponent<Rigidbody>().isKinematic = true;
            fourthShelf.GetComponent<Rigidbody>().isKinematic = true;
            trigger = false;
        }
    }  
}
