﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InstantiateBrokenModel : MonoBehaviour
{
    [SerializeField]
    GameObject _instantiateObject;

    [SerializeField]
    GameObject _unparentGameObjects;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    void OnCollisionEnter(Collision collision)
    {
        if (collision.relativeVelocity.magnitude > 2)
        {
            if(_unparentGameObjects != null)
            {
                _unparentGameObjects.transform.parent = null;
            }
            //audioSource.Play();
            if(_instantiateObject != null)
            {
                Instantiate(_instantiateObject, transform.position, transform.rotation);
                Destroy(gameObject);
            }

            if(_unparentGameObjects != null)
            {
                foreach (Transform child in _unparentGameObjects.transform)
                {
                    bool hasRigidbody = child.gameObject.GetComponent<Rigidbody>() != null;
                    if (!hasRigidbody)
                    {
                        child.gameObject.AddComponent<Rigidbody>();
                    }
                }
            }
        }
    }
}
