﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BlinkingLight : MonoBehaviour
{
    public GameObject[] emisiveObjects;
    // Start is called before the first frame update
    void Start()
    {
        StartCoroutine("flicker");
    }

    IEnumerator flicker()
    {
        while (true)
        {
            foreach (var obj in emisiveObjects)
            {
                obj.SetActive(true);
            }
            yield return new WaitForSeconds(1.0f);
            foreach (var obj in emisiveObjects)
            {
                obj.SetActive(false);
            }
            yield return new WaitForSeconds(0.5f);
        }
    }
}
