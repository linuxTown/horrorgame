﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class TriggerCollapse : MonoBehaviour
{
    //  public GameObject barrelHit;
    public GameObject shelf;
    public GameObject drawer;
    public GameObject handle;

    private void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.tag == "barrel")
        {
            UnityEngine.Debug.Log("in Collision if");
            shelf.GetComponent<CollapseShelf>().trigger = true;
            drawer.GetComponent<Rigidbody>().AddForce(new Vector3(0, 0, 1f));
            handle.SetActive(true);
        }
    }
}
