﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShelfImpact : MonoBehaviour
{
    public bool played = false;
    private void OnCollisionEnter(Collision collision)
    {
        if(collision.gameObject.tag == "audio" && played == false)
        {
            gameObject.GetComponent<AudioSource>().Play();
            played = true;
        }
    }
}
