﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class LoadScene : MonoBehaviour
{
    public void SetScene(string sceneName) // sceneName is passed from the button as a string parameter
    {
        if (sceneName != "")
            SceneManager.LoadScene(sceneName);
        else
            Debug.LogWarning("Scene name is empty");
    }

    private void Update()
    {
        // check if A button is pressed to return to menu:
        if (OVRInput.GetDown(OVRInput.RawButton.B) || Input.GetKeyDown("space"))
        {
            // Create a temporary reference to the current scene.
            Scene currentScene = SceneManager.GetActiveScene();
            // Retrieve the name of this scene.
            string sceneName = currentScene.name;
            if (sceneName != "menu") // we are currently not in menu scene
            {
                SetScene("menu");
            }
            else
            {
                Debug.Log("current scene is already menu");
            }
        }
    }
}
