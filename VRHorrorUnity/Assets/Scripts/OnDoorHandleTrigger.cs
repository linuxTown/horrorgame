﻿using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

public class OnDoorHandleTrigger : MonoBehaviour
{
    public ChangeRoom changeRoom;
    public AudioSource openDoorSound;
    public AudioSource lockedDoorSound;
    public AudioSource unlockSound;
    private bool doorLocked;

    // Start is called before the first frame update
    void Start()
    {
        doorLocked = true;
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    private void OnTriggerEnter(Collider other)
    {
        if (doorLocked)
        {
            playDoorLockedSound();
        }
        else
        {
            fadeAndTeleport();
        }
        
    }

    public void fadeAndTeleport()
    {
        if (changeRoom != null)
        {
            changeRoom.FadeOut();
            playDoorOpenSound();
        }
    }

    public void playDoorOpenSound()
    {
        if (openDoorSound != null)
        {
            openDoorSound.Play();
        }
    }

    public void playDoorLockedSound()
    {
        if (lockedDoorSound != null)
        {
            lockedDoorSound.Play();
        }
    }

    public void lockDoor(bool locked)
    {
        unlockSound.Play();
        doorLocked = locked;
    }
}

//[CustomEditor(typeof(OnDoorHandleTrigger))]
//public class playOpenDoorSoundButton : Editor
//{
//    public override void OnInspectorGUI()
//    {
//        DrawDefaultInspector();

//        OnDoorHandleTrigger myScript = (OnDoorHandleTrigger)target;
//        if (GUILayout.Button("Play open door sound"))
//        {
//            myScript.playDoorOpenSound();
//        }
//        if (GUILayout.Button("Teleport"))
//        {
//            myScript.fadeAndTeleport();
//        }
//    }

//}