﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class OpenBoxOnArmCol : MonoBehaviour
{
    [SerializeField]
    OpenCloseBox boxLidRef;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    void OnTriggerEnter(Collider other)
    {
        if(other.gameObject.name == "ArmCollider" && boxLidRef != null)
        {
            boxLidRef.OpenCloseLid();
            Destroy(other.gameObject);
        }
    }
}
